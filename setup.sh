#!/bin/sh

# check if the script is called in its source directory

([ -d Config ] && [ -d LaunchAgents ] && [ -d Utilities ]) \
|| (echo "call this script from its original directory" && exit 1)


BASE_PATH="$PWD"


# create symbolic link to the appropriate directory for every file in LaunchAgents

for LAUNCHAGENT in "${BASE_PATH}/LaunchAgents/"*.plist
do
	ln -s "$LAUNCHAGENT" ~/Library/LaunchAgents
done

for UTILITY in "${BASE_PATH}/Utilities/"*.*
do
	ln -s "$UTILITY" ~/Applications
done

# create variable for the Config directory

CONFIG_PATH="${BASE_PATH}/Config"


# create symbolic links in home directory for every file in the shell directory

for CONFIG in "${CONFIG_PATH}/shell/"*
do
	ln -s "$CONFIG" ~/".${CONFIG##*/}"
done


# if the necessary directories don’t exist, git clone them

test -e "${CONFIG_PATH}/hammerspoon" \
|| git clone https://github.com/smeikx/hammerspoon-setup.git ${CONFIG_PATH}/hammerspoon

test -e "${CONFIG_PATH}/vim" \
|| git clone https://github.com/smeikx/vim-setup.git ${CONFIG_PATH}/vim \
&& (cd ${CONFIG_PATH}/vim && ./init.sh; cd ${CONFIG_PATH})


# create symbolic links into the home directory

ln -s "${CONFIG_PATH}/vim" ~/.vim
ln -s "${CONFIG_PATH}/tmux.conf" ~/.tmux.conf
ln -s "${CONFIG_PATH}/hammerspoon" ~/.hammerspoon
ln -s "${CONFIG_PATH}/alacritty" ~/.config/


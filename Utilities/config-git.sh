#!/bin/sh

([ "$1" = "" ] || [ "$2" = "" ]) \
&& echo "Braucht Name und E-Mail als Parameter!" && exit 1

case $1 in
	*"@"*) MAIL="$1"; NAME="$2" ;;
	*) MAIL="$2"; NAME="$1" ;;
esac

git config --local user.name "$NAME"
git config --local user.email "$MAIL"

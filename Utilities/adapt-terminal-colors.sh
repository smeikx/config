#!/bin/sh

DARK_PROFILE="Hardcore"
LIGHT_PROFILE="Ayu"
PROFILE=""

if [ "$(is-darkmode.sh)" = 1 ]
then
	PROFILE="$DARK_PROFILE"
else
	PROFILE="$LIGHT_PROFILE"
fi

osascript << EOF
try
	if application "Terminal" is running then
		tell application "Terminal" to set current settings of first window to settings set "$PROFILE"
	end if
end try
EOF

#!/bin/sh

if [ "$1" != "" ]
then
	osascript -e "display notification \"$3\" with title \"$1\" subtitle \"$2\""
else
	echo "Usage: notify.sh [title] [subtitle] [content]"
fi

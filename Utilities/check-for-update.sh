#!/bin/sh

function update_and_build ()
{
	local repo_path=$1
	cd $repo_path

	local msg_title=""
	local msg_subtitle="$(basename $repo_path)"
	local msg_text=""

	git fetch
	if [ $? -ne 0 ]
	then
		if [ $(git rev-parse HEAD) != $(git rev-parse @{u}) ]
		then
			git merge --ff-only
			if [ $? -e 0 ]; then
				./update.sh
				if [ $? -e 0 ]; then
					# build succeeded
					msg_title="Update erfolgreich ❇️"
				else
					# build failed
					msg_title="Build fehlgeschlagen ❌"
				fi
			else
				# merge failed
				msg_title="Merge fehlgeschlagen ❌"
			fi

		else
			# already up to date
			return 0
		fi
	else
		# fetch failed	
		msg_title="Fetch fehlgeschlagen ❌"
	fi

	osascript -e "display notification \"$msg_text\" with title \"$msg_title\" subtitle \"$msg_subtitle\""
}

for repo in zsh godot vim; do
	update_and_build "${repo}" &
done


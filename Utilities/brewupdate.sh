#!/bin/sh

COMMANDS='update upgrade doctor'

for command in $COMMANDS
do
	ERROR_MSG=$(brew $command 2>&1 > /dev/null)
	if [ $? -ne 0 ];
	then
		FAILED_PHASE=$command
		notify.sh "🍺 Homebrew Error" "$FAILED_PHASE" "$ERROR_MSG"
		break
	fi
done


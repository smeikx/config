#!/bin/sh

# backs up files without invisible, useless system files

if [ ! $# -eq 2 ]
then
	echo "Usage: backup.sh target destination"
fi

TO_BE_BACKED_UP="$1"
BACKUP="$2"

test -d "$BACKUP" -a -d "$TO_BE_BACKED_UP" \
&& rsync -a --delete \
	--exclude=".Spotlight-V100" \
	--exclude=".Trashes" \
	--exclude=".fseventsd" \
	--exclude="*.DS_Store" \
	"$TO_BE_BACKED_UP" "$BACKUP" \
|| notify.sh "Backup Fehlgeschlagen"


#/bin/sh

CONFIG_FILE=~/.config/alacritty/alacritty.yml

if [ "$(is-darkmode.sh)" = 1 ]
then
	COLOR='*dark'
else
	COLOR='*light'
fi

sed -ie "s/^colors:.*/colors: $COLOR/" $CONFIG_FILE


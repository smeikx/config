This is some kind of über-repository that collects all my configurations files.
Some files are part of this repo, others are contained in their own, these are downloaded by `setup.sh` automatically. This script also creates symbolic links to all the required locations.

TODO: Maybe create separate repositories for the single components; the necessary data (path, URL to repo) could be stored in a proper data structure (like I did for the vim packages).

TODO: Detect if scripts are running on Linux or macOS, and adapt accordingly. Or maybe just use different branches?

